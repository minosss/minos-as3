package cc.minos.event
{
	import cc.minos.utils.ArrayUtil;
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.utils.Dictionary;
	
	public class EventProxy extends Object
	{
		private static var events:Dictionary = new Dictionary();
		
		/**
		 * 添加
		 * @param	IEDic
		 * @param	evt
		 * @param	fuc
		 */
		public static function subscribeEvent( IEDic:IEventDispatcher, evt:String, fuc:Function ):void
		{
			var ary:Array = events[ evt ];
			if ( ary == null )
			{
				ary = [];
				events[ evt ] = ary;
			}
			
			if ( !ArrayUtil.containsValue( ary, IEDic ) )
				ary.push( IEDic );
			IEDic.addEventListener( evt, fuc );
		}
		
		/**
		 * 删除
		 * @param	IEDic
		 * @param	evt
		 * @param	fuc
		 */
		public static function unsubscribeEvent( IEDic:IEventDispatcher, evt:String, fuc:Function ):void
		{
			var ary:Array = events[ evt ];
			if ( ary != null )
			{
				if ( ArrayUtil.containsValue( ary, IEDic ) )
				{
					IEDic.removeEventListener( evt, fuc );
					if ( !IEDic.hasEventListener( evt ) )
						ArrayUtil.removeValue( ary, IEDic );
				}
			}
		}
		
		/**
		 * 抛出
		 * @param	evt
		 */
		public static function broadcastEvent( evt:Event ):void
		{
			var ary:Array = events[ evt.type ];
			if ( ary != null )
			{
				ary = ArrayUtil.copyArray( ary );
				for each ( var IEDic:IEventDispatcher in ary )
				{
					if ( IEDic != null )
						IEDic.dispatchEvent( evt );
				}
				ary = null;
			}
		}
	}

}