package cc.minos.xmpp.filters
{
	import com.hurlant.util.Base64;
	
	/**
	 * ...
	 * @author Minos
	 */
	public class Base64Filter implements IFilter
	{
		
		/* INTERFACE cc.minos.xmpp.filters.IFilter */
		
		public function encode( s:String ):String
		{
			return Base64.encode( s );
		}
		
		public function decode( s:String ):String
		{
			return Base64.decode( s );
		}
	
	}

}