package cc.minos.xmpp.filters
{
	
	/**
	 * ...
	 * @author Minos
	 */
	public interface IFilter
	{
		function encode( s:String ):String;
		function decode( s:String ):String;
	}

}