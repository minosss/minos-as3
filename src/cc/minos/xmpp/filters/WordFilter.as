package cc.minos.xmpp.filters
{
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	/**
	 * ...
	 * @author Minos
	 */
	public class WordFilter
	{
		private var _reg:RegExp;
		private static var _instance:WordFilter = null;
		
		public function WordFilter( single:Single )
		{
			if ( single == null )
			{
				throw new Error( "不能实例化，这是单例类!" );
			}
			init();
		}
		
		private function init():void
		{
			_reg = new RegExp( "(#minos)" , "gi" );
			addWord( String( xml ) );
		}
		
		public function filter( str:String ):String
		{
			return str.replace( _reg , "***" );
		}
		
		public function has( s:String ):Boolean
		{
			return s.match( _reg ).length > 0;
		}
		
		public function addWord( str:String ):void
		{
			str = str.replace( /\r/g , "" );
			var arr:Array = str.split( "\n" );
			var regStr:String = "";
			for ( var i:int = 0 ; i < arr.length ; i++ )
			{
				regStr += "|" + "(" + arr[ i ] + ")";
			}
			_reg = new RegExp( _reg.source + regStr , "gi" );
		}
		
		public function loadWord( url:String ):void
		{
			var lod:URLLoader = new URLLoader();
			lod.addEventListener( Event.COMPLETE , onComplete );
			try
			{
				lod.load( new URLRequest( url ) );
			}
			catch ( er:Error )
			{
			}
		}
		
		private function onComplete( e:Event ):void
		{
			e.target.removeEventListener( Event.COMPLETE , onComplete );
			addWord( new XML( e.target.data ).toString() );
		}
		
		public static function get instance():WordFilter
		{
			if ( _instance == null )
			{
				_instance = new WordFilter( new Single() );
			}
			return _instance;
		}
	
	}

}

class Single
{
}
include "WordLibrary.inc";