package cc.minos.xmpp.filters
{
	
	/**
	 * ...
	 * @author Minos
	 */
	public class URLFilter implements IFilter
	{
		
		/* INTERFACE cc.minos.xmpp.filters.IFilter */
		
		public function encode( s:String ):String
		{
			if ( !s || s.length == 0 )
				return s;
			
			var buf:String = "";
			var tkn:Array = s.split( " " );
			for ( var i:uint = 0 ; i < tkn.length ; i++ )
			{
				var token:String = tkn[ i ].toLowerCase();
				
				if ( token.indexOf( "http://" ) == 0 || token.indexOf( "ftp://" ) == 0 || token.indexOf( "https://" ) == 0 )
				{
					buf += buildURL( tkn[ i ] );
				}
				else if ( token.indexOf( "www" ) == 0 )
				{
					var newURL:String = "http://" + tkn[ i ];
					buf += buildURL( newURL );
				}
				else
					buf += tkn[ i ];
				
				buf += " ";
			}
			return buf;
		}
		
		public function decode( s:String ):String
		{
			return s;
		}
		
		private function buildURL( url:String ):String
		{
			return "<a href='" + url + "' target='_blank'>" + url + "</a>";
		}
	}

}