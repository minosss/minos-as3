package cc.minos.tools
{
	
	/**
	 * ...
	 * @author Minos
	 */
	public class FaceChange
	{
		
		private static function change( s:String , start:String , replace:String , left:String = "" , right:String = "" ):String
		{
			var faces:Array = s.split( "\n" );
			var ns:String = "";
			var change:String;
			for each ( var i:String in faces )
			{
				if ( i.length > 5 )
				{
					change = i.substr( i.indexOf( start ) , 3 );
					i = left + i.replace( change , replace ) + right;
					ns += i;
				}
			}
			trace( ns );
			return ns;
		}
		
		public static function sogou2google( s:String ):String
		{
			return change( s , "," , "\t" , "v" );
		}
		
		public static function qq2sogou( s:String , add:int = 5 ):String
		{
			return change( s , "=" , ",5=" );
		}
		
		public static function sogou2qq( s:String , add:int = 5 ):String
		{
			return change( s , "," , "=5," );
		}
	
	}

}