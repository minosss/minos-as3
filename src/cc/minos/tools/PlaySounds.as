package cc.minos.tools
{
	import com.greensock.loading.MP3Loader;
	import flash.utils.Dictionary;
	
	/**
	 * ...
	 * 播放文件夹下面的声音
	 * 使用第三方greensock的MP3Loader类加载
	 * @author Minos
	 */
	public class PlaySounds
	{
		//声音路径
		private var soundPath:String = "";
		//储存声音
		private var sounds:Dictionary;
		//当前声音加载器
		private var _currentLoader:MP3Loader;
		
		/**
		 * 加载路径:path+key
		 * @param	soundPath		:	文件夹
		 */
		public function PlaySounds( soundPath:String = "" )
		{
			this.soundPath = soundPath;
			sounds = new Dictionary();
		}
		
		/**
		 * 播放
		 * @param	key				:	文件名
		 */
		public function play( key:String ):void
		{
			
			if ( key == null || key == "" )
				return;
			if ( _currentLoader )
				_currentLoader.pauseSound();
				
			_currentLoader = getLoader( key , true );
			_currentLoader.load();
		}
		
		/**
		 * 加载
		 * @param	key				:	文件名
		 */
		public function load( key:String ):void
		{
			if ( key == null || key == "" )
				return;
			getLoader( key , false ).load();
		}
		
		/**
		 * 清空
		 */
		public function clear():void
		{
			for ( var str:Object in sounds )
			{
				sounds[ str ] = null;
			}
			sounds = new Dictionary();
		}
		
		/**
		 * 停止
		 */
		public function stop():void
		{
			if ( _currentLoader )
				_currentLoader.pauseSound();
		}
		
		/**
		 * 获取加载器
		 * @param	key				:	文件名
		 * @param	autoPlay		:	是否自动播放
		 * @return	加载器
		 */
		private function getLoader( key:String , autoPlay:Boolean = true ):MP3Loader
		{
			var m3:MP3Loader = sounds[ key ];
			if ( m3 == null )
			{
				m3 = new MP3Loader( soundPath + key , { name: key , autoPlay: autoPlay } );
				sounds[ key ] = m3;
			}
			return m3;
		}
	
	}

}