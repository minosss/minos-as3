package cc.minos.graphics
{
	
	/**
	 * ...
	 * @author Minos
	 */
	public interface IPunctuation
	{
		function set punctuation( c:Class ):void;
		function get punctuation():Class;
		function updateIcon():void;
	}

}