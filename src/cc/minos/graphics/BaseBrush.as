package cc.minos.graphics
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.ColorTransform;
	
	/**
	 * ...
	 * @author Minos
	 */
	public class BaseBrush extends BaseGraphics
	{
		
		protected var _randomRotation:Boolean;
		protected var _dot:Class;
		protected var _gap:Number;
		protected var _colorTf:ColorTransform;
		
		/**
		 * 
		 * @param	color				:	颜色
		 * @param	gap					:	密度
		 * @param	dot					:	点类型
		 * @param	randomRotation		:	是否旋转
		 */
		public function BaseBrush( color:uint , gap:Number , dot:Class , randomRotation:Boolean = false )
		{
			super();
			_type = "BaseBrush";
			_gap = gap;
			_dot = dot;
			_randomRotation = randomRotation;
			_colorTf = new ColorTransform();
			_colorTf.color = color;
		}
		
		/**
		 * 起始位置
		 * @param	xpos
		 * @param	ypos
		 */
		override public function start( xpos:Number , ypos:Number ):void
		{
			super.start( xpos , ypos );
		}
		
		/**
		 * 画图
		 * @param	xpos
		 * @param	ypos
		 */
		override public function draw( xpos:Number , ypos:Number ):void
		{
			super.draw( xpos , ypos );
			var dx:Number = xpos - startX;
			var dy:Number = ypos - startY;
			var dis:Number = Math.sqrt( dx * dx + dy * dy );
			var times:int = Math.ceil( dis / _gap );
			for ( var i:int = 0 ; i < times ; i++ )
			{
				var d:* = new _dot();
				d.x = dx / times * i + startX;
				d.y = dy / times * i + startY;
				d.transform.colorTransform = _colorTf;
				if ( _randomRotation )
				{
					d.rotation = Math.random() * 360;
				}
				addChild( d );
			}
		}
		
		/**
		 * 转换成位图
		 * @param	w
		 * @param	h
		 */
		public function changeToBitmap( w:Number , h:Number ):void
		{
			var bmd:BitmapData = new BitmapData( w , h , true , 0 );
			bmd.draw( this );
			var bmp:Bitmap = new Bitmap( bmd );
			while ( numChildren > 0 )
			{
				removeChildAt( 0 );
			}
			addChild( bmp );
		}
	
	}

}