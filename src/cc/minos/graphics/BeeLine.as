package cc.minos.graphics
{
	
	/**
	 * ...
	 * @author Minos
	 */
	public class BeeLine extends Line
	{
		
		public function BeeLine()
		{
			super();
			_type = GraphicsFactroy.BEELINE;
		}
		
		override public function draw( xpos:Number , ypos:Number ):void
		{
			this.graphics.clear();
			start( startX , startY );
			this.graphics.lineTo( xpos , ypos );
		}
	
	}

}