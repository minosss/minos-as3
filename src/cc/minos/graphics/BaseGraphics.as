package cc.minos.graphics
{
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author Minos
	 */
	public class BaseGraphics extends Sprite implements IColorAble , ISyncAble , IThicknessAble
	{
		//粗细
		protected var _thickness:Number = 1.0;
		//是否填充
		protected var _isColor:Boolean;
		//颜色
		protected var _color:uint = 0x000000;
		//类型
		protected var _type:String;
		//起始位置X
		protected var startX:Number;
		//起始位置Y
		protected var startY:Number;
		
		/**
		 *
		 */
		public function BaseGraphics()
		{
			_type = "BaseGraphics";
			//this.mouseEnabled = false;
			this.mouseChildren = false;
		}
		
		/**
		 * to override
		 * @param	xpos
		 * @param	ypos
		 */
		public function start( xpos:Number , ypos:Number ):void
		{
			startX = xpos;
			startY = ypos;
		}
		
		/**
		 * to override
		 * @param	xpos
		 * @param	ypos
		 */
		public function draw( xpos:Number , ypos:Number ):void
		{
		}
		
		public function get type():String
		{
			return _type;
		}
		
		/* INTERFACE cc.minos.graphics.IColorAble */
		
		public function get color():uint
		{
			return _color;
		}
		
		public function set color( value:uint ):void
		{
			//_isColor = value != null;
			_color = value;
		}
		
		public function get isColor():Boolean
		{
			return _isColor;
		}
		
		public function set isColor( value:Boolean ):void {
			_isColor = value;
		}
		
		/* INTERFACE cc.minos.graphics.ISyncAble */
		
		public function setSyncData( data:* ):void
		{
		}
		
		public function getSyncData():*
		{
			return null;
		}
		
		/* INTERFACE cc.minos.graphics.IThicknessAble */
		
		/**
		 * 粗细
		 */
		public function set thickness( value:Number ):void
		{
			_thickness = value;
		}
		
		/**
		 * 粗细
		 */
		public function get thickness():Number
		{
			return _thickness;
		}
	
	}

}