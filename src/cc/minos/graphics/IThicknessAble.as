package cc.minos.graphics
{
	
	/**
	 * ...
	 * @author Minos
	 */
	public interface IThicknessAble
	{
		function set thickness( v:Number ):void
		function get thickness():Number;
	}

}