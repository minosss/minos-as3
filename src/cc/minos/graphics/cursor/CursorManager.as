package cc.minos.graphics.cursor
{
	import cc.minos.graphics.GraphicsFactroy;
	import cc.minos.graphics.icons.Punctuation;
	import cc.minos.utils.FilterUtil;
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.filters.DropShadowFilter;
	import flash.ui.Mouse;
	import flash.ui.MouseCursor;
	import flash.utils.Timer;
	
	/**
	 * ...
	 * @author Minos
	 */
	public class CursorManager
	{
		private static var cursor:Shape = new Shape();
		private static var isDrag:Boolean = false;
		
		public static function show( type:String , size:Number = 0 ):Shape
		{
			if ( !isDrag )
				Mouse.hide();
			cursor.filters = [ FilterUtil.getGlow() ];
			//
			if ( Punctuation.matches( type ) ) {
				type = GraphicsFactroy.PUNCTUATION_LABEL;
			}
			//
			switch ( type )
			{
				case GraphicsFactroy.LINE: 
				case GraphicsFactroy.BEELINE: 
				case GraphicsFactroy.RECT: 
				case GraphicsFactroy.TRIANGLE: 
				case GraphicsFactroy.PARALLELOGRAM: 
				case GraphicsFactroy.POLYGON: 
				case GraphicsFactroy.CIRCLE: 
					return getLineCursor();
					break;
				case GraphicsFactroy.ERASER: 
					return getBrushCursor( size );
					break;
				case GraphicsFactroy.LABEL: 
				case GraphicsFactroy.PUNCTUATION_LABEL:
					Mouse.show();
					Mouse.cursor = MouseCursor.IBEAM;
					break;
				case "drag": 
					Mouse.show();
					Mouse.cursor = MouseCursor.HAND;
					break;
			}
			return new Shape();
		}
		
		public static function move( x:Number = 0 , y:Number = 0 ):void
		{
			cursor.x = x;
			cursor.y = y;
		}
		
		public static function drag( b:Boolean ):void
		{
			isDrag = b;
			cursor.visible = !b;
			if ( b )
			{
				Mouse.cursor = MouseCursor.HAND;
				Mouse.show();
			}
			else
			{
				Mouse.cursor = MouseCursor.AUTO;
				if ( cursor.filters.length == 0 )
				{
					Mouse.show();
				}
				else
				{
					Mouse.hide();
				}
			}
		}
		
		public static function hide():void
		{
			cursor.graphics.clear();
			cursor.filters = [];
			if ( !isDrag )
			{
				Mouse.cursor = MouseCursor.AUTO;
				Mouse.show();
			}
		}
		
		/**
		 * 
		 * @param	size
		 * @return
		 */
		public static function getBrushCursor( size:Number ):Shape
		{
			cursor.graphics.clear();
			cursor.graphics.lineStyle( 1 , 0 , 1 );
			cursor.graphics.drawCircle( 0 , 0 , size );
			return cursor;
		}
		
		/**
		 * 
		 * @param	padding
		 * @param	lenght
		 * @return
		 */
		public static function getLineCursor( padding:Number = 3 , lenght:Number = 5 ):Shape
		{
			cursor.graphics.clear();
			cursor.graphics.lineStyle( 1 , 0 , 1 , false , "normal" , "none" );
			cursor.graphics.moveTo( -padding , 0 );
			cursor.graphics.lineTo( -padding - lenght , 0 );
			cursor.graphics.moveTo( 0 , -padding );
			cursor.graphics.lineTo( 0 , -padding - lenght );
			cursor.graphics.moveTo( padding , 0 );
			cursor.graphics.lineTo( padding + lenght , 0 );
			cursor.graphics.moveTo( 0 , padding );
			cursor.graphics.lineTo( 0 , padding + lenght );
			return cursor;
		}
	
	}

}