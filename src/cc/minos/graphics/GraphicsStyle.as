package cc.minos.graphics
{
	
	/**
	 * ...
	 * @author Minos
	 */
	public class GraphicsStyle
	{
		/**
		 * 获取边数范围
		 * @param	type
		 * @return	a vector.<int>
		 */
		public static function getSidesRange( type:String ):Vector.<int>
		{
			var v:Vector.<int> = new Vector.<int>( 2 );
			
			switch ( type )
			{
				case GraphicsFactroy.POLYGON: 
					v[ 0 ] = 3;
					v[ 1 ] = 32;
					break;
				default: 
					v[ 0 ] = 0;
					v[ 1 ] = 0;
			}
			return v;
		}
		
		/**
		 * 获取字体大小范围
		 * @param	type
		 * @return	a vector.<int>
		 */
		public static function getFontSizeRange( type:String ):Vector.<int>
		{
			var v:Vector.<int> = new Vector.<int>( 2 );
			
			switch ( type )
			{
				case GraphicsFactroy.LINE: 
				case GraphicsFactroy.BEELINE: 
				case GraphicsFactroy.RECT: 
				case GraphicsFactroy.TRIANGLE: 
				case GraphicsFactroy.PARALLELOGRAM: 
				case GraphicsFactroy.POLYGON: 
					v[ 0 ] = 1;
					v[ 1 ] = 100;
					break;
				case GraphicsFactroy.ERASER: 
					v[ 0 ] = 20;
					v[ 1 ] = 50;
					break;
				default: 
					v[ 0 ] = 12;
					v[ 1 ] = 48;
			}
			return v;
		}
	
	}

}