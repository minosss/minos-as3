package cc.minos.graphics
{
	
	/**
	 * ...
	 * @author Minos
	 */
	public class Rect extends BaseGraphics implements IFillColorAble
	{
		protected var _fillColor:uint = 0xffffff;
		protected var _isFill:Boolean;
		
		public function Rect()
		{
			super();
			_type = GraphicsFactroy.RECT;
		}
		
		override public function draw( xpos:Number , ypos:Number ):void
		{
			super.draw( xpos , ypos );
			this.graphics.clear();
			this.graphics.lineStyle( _thickness , _color );
			if ( isFill )
				this.graphics.beginFill( _fillColor , 1 );
			this.graphics.drawRect( startX , startY , xpos - startX , ypos - startY );
			this.graphics.endFill();
		}
		
		/* INTERFACE cc.minos.graphics.IFillColorAble */
		
		public function set fillColor( value:uint ):void
		{
			_fillColor = value;
		}
		
		public function get fillColor():uint
		{
			return _fillColor;
		}
		
		public function get isFill():Boolean 
		{
			return _isFill;
		}
		
		public function set isFill(value:Boolean):void 
		{
			_isFill = value;
		}
		
	
	}

}