package cc.minos.graphics
{
	
	/**
	 * ...
	 * @author Minos
	 */
	public interface ISyncAble
	{
		function setSyncData( data:* ):void;
		function getSyncData():*;
	}

}