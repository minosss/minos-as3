package cc.minos.graphics
{
	
	/**
	 * ...
	 * @author Minos
	 */
	public interface IColorAble
	{
		function get color():uint;
		function set color( c:uint ):void;
		function get isColor():Boolean;
		function set isColor( b:Boolean ):void;
	}

}