package cc.minos.graphics
{
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import flash.ui.Keyboard;
	
	/**
	 * ...
	 * @author Minos
	 */
	public class Label extends BaseGraphics
	{
		
		protected var _tf:TextField;
		
		public function Label()
		{
			super();
			_type = GraphicsFactroy.LABEL;
			mouseChildren = true;
		}
		
		override public function start( xpos:Number , ypos:Number ):void
		{
			super.start( xpos , ypos );
			
			_tf = new TextField();
			_tf.text = "";
			_tf.defaultTextFormat = new TextFormat( "宋体" , thickness );
			_tf.textColor = color;
			_tf.autoSize = TextFieldAutoSize.LEFT;
			_tf.type = TextFieldType.INPUT;
			_tf.addEventListener( FocusEvent.FOCUS_OUT , onTfFocusOut );
			_tf.addEventListener( KeyboardEvent.KEY_DOWN , onTfKeyDown );
			_tf.x = xpos;
			_tf.y = ypos - _tf.height;
			addChild( _tf );
			
			if ( stage )
				stage.focus = _tf;
		}
		
		private function onTfKeyDown( e:KeyboardEvent ):void
		{
			if ( e.keyCode == Keyboard.ENTER ) {
				onTfFocusOut();
			}
		}
		
		private function onTfFocusOut( e:FocusEvent = null ):void
		{
			_tf.removeEventListener( KeyboardEvent.KEY_DOWN , onTfKeyDown );
			_tf.removeEventListener( FocusEvent.FOCUS_OUT , onTfFocusOut );
			_tf.type = TextFieldType.DYNAMIC;
			_tf.selectable = false;
			
			if ( _tf.text == "" )
			{
				removeChild( _tf );
			}
		}
		
		override public function set color( value:uint ):void
		{
			super.color = value;
			if ( _tf )
				_tf.textColor = color;
		}
		
		override public function draw( xpos:Number , ypos:Number ):void
		{
			super.draw( xpos , ypos );
		}
	
	}

}