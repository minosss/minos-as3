package cc.minos.graphics
{
	import flash.geom.Point;
	
	/**
	 * ...
	 * @author Minos
	 */
	public class Triangle extends Rect
	{
		protected var _side:uint = 3;
		
		public function Triangle()
		{
			super();
			_type = GraphicsFactroy.TRIANGLE;
		}
		
		override public function draw( xpos:Number , ypos:Number ):void
		{
			var startP:Point = new Point( startX , startY );
			var mouseP:Point = new Point( xpos , ypos );
			var radius:Number = Point.distance( startP , mouseP );
			var startAngle:Number = Math.atan2( ypos - startY , xpos - startX )
			
			var commands:Vector.<int> = Vector.<int>([ 1 ] );
			var data:Vector.<Number> = Vector.<Number>([] );
			
			this.graphics.clear();
			this.graphics.lineStyle( _thickness , _color );
			if ( isFill )
				this.graphics.beginFill( _fillColor , 1 );
			var n:int = _side + 1;
			for ( var i:int = 0 ; i < n ; i++ )
			{
				var angle:Number = Math.PI * 2 / ( n - 1 ) * i + startAngle;
				var nx:Number = Math.cos( angle ) * radius + startX;
				var ny:Number = Math.sin( angle ) * radius + startY;
				if ( i > 0 )
					commands.push( 2 );
				data.push( nx , ny );
			}
			this.graphics.drawPath( commands , data );
			this.graphics.endFill();
		}
	}
}