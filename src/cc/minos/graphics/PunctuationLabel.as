package cc.minos.graphics
{
	import cc.minos.console.Console;
	import cc.minos.graphics.icons.Punctuation;
	import flash.display.DisplayObject;
	
	/**
	 * ...
	 * @author Minos
	 */
	public class PunctuationLabel extends Label implements IPunctuation
	{
		
		private var _icon:DisplayObject;
		
		private var _punctuation:Class;
		public function PunctuationLabel()
		{
			super();
			_type = GraphicsFactroy.PUNCTUATION_LABEL;
		}
		
		override public function start( xpos:Number , ypos:Number ):void
		{
			super.start( xpos , ypos );
			updateIcon();
		}
		
		/* INTERFACE cc.minos.graphics.IPunctuation */
		
		public function set punctuation( value:Class ):void
		{
			_punctuation = value;
		}
		
		public function get punctuation():Class
		{
			return _punctuation;
		}
		
		public function updateIcon():void
		{
			if ( !punctuation ) return;
			if ( !_icon ) {
				//new
				_icon = new punctuation();
				addChild( _icon );
			}
			//update scale
			_icon.scaleX = _icon.scaleY = thickness / 12; 
			//update position
			if ( _tf )
			{
				_icon.x = _tf.x - _icon.width;
				_icon.y = _tf.y + (_tf.height - _icon.height)*.5;
			}
		}
		
		override public function get thickness():Number
		{
			return super.thickness;
		}
		
		override public function set thickness( value:Number ):void
		{
			super.thickness = value;
			//change icon scael
			updateIcon();
		}
	
	}

}