package cc.minos.graphics
{
	import cc.minos.graphics.icons.Punctuation;
	
	/**
	 * ...
	 * @author Minos
	 */
	public class GraphicsFactroy
	{
		
		static public const LINE:String = "line"; //线条
		static public const PENCIL:String = "pencil"; //铅笔
		static public const CRAYON:String = "crayon"; //蜡笔
		static public const WATER_BRUSH:String = "waterBrush"; //水彩
		static public const ERASER:String = "eraser"; //橡皮
		static public const CIRCLE:String = "circle"; //圆形
		static public const RECT:String = "rect"; //矩形
		static public const POLYGON:String = "polygon"; //多边
		static public const STAR:String = "star"; //星星
		static public const BEELINE:String = "beeline"; //直线
		static public const LABEL:String = "label"; //文本
		static public const TRIANGLE:String = "triangle"; //三角
		static public const PARALLELOGRAM:String = "parallelogram"; //平行四边
		static public const PUNCTUATION_LABEL:String = "punctuationLabel"; //符号
		
		public static function getClass( type:String ):Class
		{
			//
			if ( Punctuation.matches( type ) )
			{
				type = PUNCTUATION_LABEL;
			}
			//
			switch ( type )
			{
				case GraphicsFactroy.BEELINE: 
					return BeeLine
					break;
				case GraphicsFactroy.LINE: 
					return Line;
					break;
				//case GraphicsFactroy.PENCIL: 
				//return Pencil;
				//break;
				//case GraphicsFactroy.CRAYON: 
				//return Crayon;
				//break;
				//case GraphicsFactroy.WATER_BRUSH: 
				//return WaterBrush;
				//break;
				case GraphicsFactroy.ERASER: 
					return Eraser;
					break;
				case GraphicsFactroy.CIRCLE: 
					return Circle;
					break;
				case GraphicsFactroy.RECT: 
					return Rect;
					break;
				case GraphicsFactroy.POLYGON: 
					return Polygon;
					break;
				case GraphicsFactroy.STAR: 
					return Star;
					break;
				case GraphicsFactroy.LABEL: 
					return Label;
					break;
				case GraphicsFactroy.TRIANGLE: 
					return Triangle;
					break;
				//case GraphicsFactroy.PARALLELOGRAM: 
				//return Parallelogram;
				//break;
				case GraphicsFactroy.PUNCTUATION_LABEL: 
					return PunctuationLabel;
					break;
			}
			return null;
		}
	
	}

}