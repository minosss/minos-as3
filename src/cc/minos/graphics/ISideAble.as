package cc.minos.graphics
{
	
	/**
	 * ...
	 * @author Minos
	 */
	public interface ISideAble
	{
		function get side():int;
		function set side( s:int ):void;
	}

}