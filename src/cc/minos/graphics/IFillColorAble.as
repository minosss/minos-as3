package cc.minos.graphics
{
	
	/**
	 * ...
	 * @author Minos
	 */
	public interface IFillColorAble
	{
		function set fillColor( v:uint ):void;
		function get fillColor():uint;
		function get isFill():Boolean;
		function set isFill( b:Boolean ):void;
	}

}