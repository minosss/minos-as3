package cc.minos.graphics
{
	
	/**
	 * ...
	 * @author Minos
	 */
	public class Eraser extends Line
	{
		/**
		 * 橡皮
		 */
		public function Eraser()
		{
			super();
			_type = GraphicsFactroy.ERASER;
		}
		
		override public function start( xpos:Number , ypos:Number ):void
		{
			super.start( xpos , ypos );
			this.graphics.lineStyle( thickness * 2 , 0xFFFFFF );
			this.graphics.moveTo( xpos , ypos );
		}
	
	}

}