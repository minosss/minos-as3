package cc.minos.graphics.icons
{
	import flash.utils.getDefinitionByName;
	
	/**
	 * ...
	 * @author Minos
	 */
	public class Punctuation
	{
		public var rightAngle:Class;
		
		public var lessThan:Class;
		
		public var greaterThan:Class;
		
		private static var punctuations:Array = [ "rightAngle" , "lessThan" , "greaterThan" ];
		
		public function Punctuation()
		{
		}
		
		public static function getArray():Array
		{
			return punctuations
		}
		
		public static function matches( t:String ):Boolean
		{
			return punctuations.indexOf( t ) != -1;
		}
		
		public static function getClass( t:String ):Class
		{
			if ( t == null || t == "" )
				return null;
			return getDefinitionByName(t) as Class;
		}
	
	}

}