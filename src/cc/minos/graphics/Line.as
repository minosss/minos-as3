package cc.minos.graphics
{
	import flash.geom.Point;
	
	/**
	 * ...
	 * @author Minos
	 */
	public class Line extends BaseGraphics
	{
		
		/**
		 */
		public function Line()
		{
			super();
			_type = GraphicsFactroy.LINE;
		}
		
		/**
		 * 开始
		 * @param	xpos
		 * @param	ypos
		 */
		override public function start( xpos:Number , ypos:Number ):void
		{
			super.start( xpos , ypos );
			this.graphics.lineStyle( _thickness , _color );
			this.graphics.moveTo( xpos , ypos );
		}
		
		/**
		 * 画图
		 * @param	xpos
		 * @param	ypos
		 */
		override public function draw( xpos:Number , ypos:Number ):void
		{
			super.draw( xpos , ypos );
			this.graphics.lineTo( xpos , ypos );
			startX = xpos;
			startY = ypos;
		}
		
		/**
		 *
		 * @param	data
		 */
		override public function setSyncData( data:* ):void
		{
		}
	
	}
}