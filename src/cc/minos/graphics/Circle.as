package cc.minos.graphics
{
	
	/**
	 * ...
	 * @author Minos
	 */
	public class Circle extends Rect
	{
		
		public function Circle()
		{
			super();
			_type = GraphicsFactroy.CIRCLE;
		}
		
		override public function draw( xpos:Number , ypos:Number ):void
		{
			//super.draw(xpos, ypos);
			this.graphics.clear();
			this.graphics.lineStyle( _thickness , _color );
			if ( isFill )
				this.graphics.beginFill( _fillColor , 1 );
			this.graphics.drawEllipse( startX , startY , xpos - startX , ypos - startY );
			this.graphics.endFill();
			
			
		}
	
	}

}