package cc.minos.graphics
{
	import flash.geom.Point;
	
	/**
	 * ...
	 * @author Minos
	 */
	public class Polygon extends Triangle implements ISideAble
	{
		protected var _isStar:Boolean = false;
		
		public function Polygon()
		{
			super();
			_type = GraphicsFactroy.POLYGON;
		}
		
		override public function draw( xpos:Number , ypos:Number ):void
		{
			var startP:Point = new Point( startX , startY );
			var mouseP:Point = new Point( xpos , ypos );
			var radius:Number = Point.distance( startP , mouseP );
			if ( _isStar )
			{
				var radius1:Number = radius;
				var radius2:Number = radius * ( .3 + _side / 40 );
			}
			var startAngle:Number = Math.atan2( ypos - startY , xpos - startX )
			var commands:Vector.<int> = Vector.<int>([ 1 ] );
			var data:Vector.<Number> = Vector.<Number>([] );
			
			this.graphics.clear();
			this.graphics.lineStyle( _thickness , _color );
			if ( isFill )
				this.graphics.beginFill( _fillColor , 1 );
			var n:int = _isStar ? _side * 2 + 1 : _side + 1;
			for ( var i:int = 0 ; i < n ; i++ )
			{
				if ( _isStar )
				{
					radius = ( radius == radius1 ) ? radius2 : radius1;
				}
				var angle:Number = Math.PI * 2 / ( n - 1 ) * i + startAngle;
				var nx:Number = Math.cos( angle ) * radius + startX;
				var ny:Number = Math.sin( angle ) * radius + startY;
				
				if ( i > 0 )
					commands.push( 2 );
				data.push( nx , ny );
			}
			this.graphics.drawPath( commands , data );
			this.graphics.endFill();
		}
		
		/* INTERFACE cc.minos.graphics.ISideAble */
		
		public function get side():int
		{
			return _side;
		}
		
		public function set side( value:int ):void
		{
			_side = value;
		}
	
	}

}