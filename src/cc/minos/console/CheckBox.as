package cc.minos.console
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.ui.Keyboard;
	
	/**
	 * ...
	 * @author Minos
	 */
	public class CheckBox extends Sprite
	{
		private var input:TextField;
		private var psw:String;
		
		public function CheckBox( p:String = 'minos.' )
		{
			psw = p;
			input = new TextField();
			input.defaultTextFormat = new TextFormat( "Consolas" , 12 , 0 , true );
			input.width = 100 , input.height = 20;
			input.x = 10 , input.y = 10;
			input.background = true;
			input.backgroundColor = 0xffffff;
			input.type = 'input';
			input.addEventListener( KeyboardEvent.KEY_DOWN , onKeyDown );
			
			addChild( input );
			
			//darw
			this.graphics.clear();
			this.graphics.beginFill( 0x333333 , 1 );
			this.graphics.drawRect( 0 , 0 , 120 , 40 );
			this.graphics.endFill();
		}
		
		private function onKeyDown( e:KeyboardEvent ):void
		{
			if ( e.keyCode == Keyboard.ENTER )
			{
				input.stage.focus = null;
				dispatchEvent( new Event( 'check' ) );
				if ( isPass )
				{
					input.removeEventListener( KeyboardEvent.KEY_DOWN , onKeyDown );
				}else {
					input.text = '';
				}
			}
		}
		
		public function get isPass():Boolean
		{
			return input.text == psw;
			//return true;
		}
	
	}

}