package cc.minos.console
{
	import flash.system.Capabilities;
	
	/**
	 * ...
	 * @author Minos
	 */
	public class ConsoleSettting
	{
		
		public static var author:String = "Minos";
		public static var font:String = "Consolas";
		public static var size:Number = 12;
		
		public static var alcon:Boolean = false;
		public static var chrome:Boolean = false;
		
		public static const VERSION:String = "1.12";
		
		public static const TITLE_COLOR:uint = 0xFFFFFF;
		public static const TITLE_BACKGROUND_COLOR:uint = 0x333333;
		
		public static const CONSOLE_COLOR:uint = 0x000000;
		public static const CONSOLE_BORDER_COLOR:uint = 0x333333;
		public static const CONSOLE_BACKGROUND_COLOR:uint = 0xffffff;
		
		public static const BACKGROUND_COLOR:uint = 0xffffff;
		public static const BACKGROUND_BORDER_COLOR:uint = 0x000000;
	
	}

}