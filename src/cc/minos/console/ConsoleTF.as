package cc.minos.console
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	/**
	 * ...
	 * @author Minos
	 */
	public class ConsoleTF extends Sprite
	{
		private var tf:TextField;
		
		public function ConsoleTF()
		{
			//super();
			addChild( tf = new TextField );
			tf.defaultTextFormat = new TextFormat( ConsoleSettting.font , ConsoleSettting.size , ConsoleSettting.CONSOLE_COLOR );
			tf.multiline = true;
			tf.wordWrap = true;
			tf.border = true;
			tf.borderColor = ConsoleSettting.CONSOLE_BORDER_COLOR;
			tf.background = true;
			tf.backgroundColor = ConsoleSettting.CONSOLE_BACKGROUND_COLOR;
		}
		
		override public function get width():Number
		{
			return super.width;
		}
		
		override public function set width( value:Number ):void
		{
			tf.width = value;
		}
		
		override public function get height():Number
		{
			return super.height;
		}
		
		override public function set height( value:Number ):void
		{
			tf.height = value;
		}
		
		public function get numLines():int
		{
			return tf.numLines;
		}
		
		public function set text( t:String ):void
		{
			tf.text = t;
			dispatchEvent( new Event( Event.CHANGE ) );
		}
		
		public function set htmlText( ht:String ):void
		{
			tf.htmlText = ht;
			dispatchEvent( new Event( Event.CHANGE ) );
		}
		
		public function get htmlText():String
		{
			return tf.htmlText;
		}
		
		/* INTERFACE cc.minos.components.IScrollAbleV */
		
		public function get scrollV():Number
		{
			return Number( tf.scrollV );
		}
		
		public function set scrollV( value:Number ):void
		{
			tf.scrollV = int( value );
		}
		
		public function get maxScrollV():Number
		{
			return Number( tf.maxScrollV );
		}
		
		public function get contentHeight():Number
		{
			return Number( tf.numLines );
		}
	
	}

}