package cc.minos.console
{
	
	/**
	 * ...
	 * @author Minos
	 */
	public interface ICommand
	{
		function addCommand( cmd:String, action:Function ):void;
		function removeCommand( cmd:String ):void;
		function removeAllCommand():void;
	}

}