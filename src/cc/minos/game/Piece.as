package cc.minos.game
{
	import flash.display.Sprite;
	
	/**
	 * ...
	 * 方块基类
	 * @author Minos
	 */
	public class Piece extends Sprite
	{
		public var col:uint;
		public var row:uint;
		
		protected var _type:int;
		protected var _selected:Boolean;
		
		public function Piece()
		{
		}
		
		/**
		 * 渲染
		 */
		protected function draw():void
		{
		}
		
		/**
		 * 类型
		 */
		public function get type():int
		{
			return _type;
		}
		
		public function set type( value:int ):void
		{
			_type = value;
			draw();
		}
		
		/**
		 * 选中
		 */
		public function get selected():Boolean
		{
			return _selected;
		}
		
		public function set selected( value:Boolean ):void
		{
			_selected = value;
			draw();
		}
		
		/**
		 * 消失
		 */
		public function destroy():void
		{
			parent.removeChild( this );
		}
		
		/**
		 * 输出信息
		 * @return
		 */
		override public function toString():String
		{
			return ' [' + col + ',' + row + ',' + type + '] ';
		}
	}

}