package cc.minos.game
{
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	import flash.events.TimerEvent;
	import flash.ui.Keyboard;
	
	/**
	 * ...
	 * 俄罗斯方块
	 * @author Minos
	 */
	public class Tetris extends GridGame
	{
		
		private var isDropping:Boolean;
		private var movePiece:Piece;
		
		public function Tetris( continer:Sprite , cols:int , rows:int , spacing:Number , pieceClass:Class )
		{
			super( continer , cols , rows , spacing , pieceClass , 1 );
			isDropping = false;
		}
		
		override protected function setUpGrid():void
		{
			continer.graphics.clear();
			continer.graphics.lineStyle( 2 , 0 , .6 , true );
			continer.graphics.drawRect( 0 , 0 , cols * spacing , rows * spacing )
		
		}
		
		override protected function onTimer( e:TimerEvent ):void
		{
			move( 0 , 1 );
		}
		
		override public function start():void
		{
			super.start();
			continer.stage.addEventListener( KeyboardEvent.KEY_DOWN , onKeyDown );
		}
		
		override public function stop():void
		{
			super.stop();
			continer.stage.removeEventListener( KeyboardEvent.KEY_DOWN , onKeyDown );
		}
		
		private function onKeyDown( e:KeyboardEvent ):void
		{
			switch ( e.keyCode )
			{
				case Keyboard.UP: 
					trace( 'change' );
					break;
				case Keyboard.DOWN: 
					break;
				case Keyboard.LEFT: 
					move( -1 , 0 );
					break;
				case Keyboard.RIGHT: 
					move( 1 , 0 );
					break;
			}
		}
		
		private function move( col:int , row:int ):void
		{
			//check 
			
			
			//move
			movePiece.col += col;
			movePiece.row += row;
			movePiece.x = movePiece.col * spacing;
			movePiece.y = movePiece.row * spacing;
			
		}
	}

}