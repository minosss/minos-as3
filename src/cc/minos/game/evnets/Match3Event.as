package cc.minos.game.evnets
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Minos
	 */
	public class Match3Event extends Event
	{
		public static const GAME_LOSE:String = 'game-lose';
		public static const GAME_WIN:String = 'game-win';
		
		public function Match3Event( type:String , bubbles:Boolean = false , cancelable:Boolean = false )
		{
			super( type , bubbles , cancelable );
		}
		
		public override function clone():Event
		{
			return new Match3Event( type , bubbles , cancelable );
		}
		
		public override function toString():String
		{
			return formatToString( "Match3Event" , "type" , "bubbles" , "cancelable" , "eventPhase" );
		}
	
	}

}