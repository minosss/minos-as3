package cc.minos.game.go
{
	import mx.utils.StringUtil;
	
	/**
	 * ...
	 * 围棋字符串处理
	 * @author Minos
	 */
	public class SGFUtil
	{
		/**
		 * 删除回车
		 * @param	s
		 * @return
		 */
		public static function removeReturn( s:String ):String
		{
			return s.split( "\r" ).join( "" ).split( "\n" ).join( "" );
		}
		
		/**
		 * 是否匹配
		 * @param	s1
		 * @param	s2
		 * @return
		 */
		public static function match( s1:String , s2:String ):Boolean
		{
			return s1 != null && s2 != null && s1 == s2;
		}
		
		/**
		 * 是否空格
		 * @param	s
		 * @return
		 */
		public static function isWhitespace( s:String ):Boolean
		{
			return StringUtil.isWhitespace( s );
		}
		
		//字母顺序
		private static const LETTERS:String = "abcdefghijklmnopqrstuvwxyz";
		
		/**
		 * 字母转数字
		 * @param	s
		 * @return
		 */
		public static function l2Int( s:String ):int
		{
			return LETTERS.indexOf( s.toLowerCase() );
		}
		
		/**
		 * 数字转字母
		 * @param	i
		 * @return
		 */
		public static function i2Letter( i:int ):String
		{
			return LETTERS.charAt( i );
		}
	
	}

}