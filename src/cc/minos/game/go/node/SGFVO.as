package cc.minos.game.go.node
{
	import cc.minos.mgo.SGFUtil;
	
	/**
	 * ...
	 * 棋子
	 * @author Minos
	 */
	public class SGFVO
	{
		public var nodeColor:int = 0;
		public var nodeX:int = -1;
		public var nodeY:int = -1;
		public var comment:Array;
		
		public function SGFVO()
		{
		}
		
		public function toString():String
		{
			return "[ x:" + SGFUtil.i2Letter( nodeX ) + ", y:" + (9-nodeY) + ", c:" + (nodeColor==1?"W":"B") + " ]";
		}
	
	}

}