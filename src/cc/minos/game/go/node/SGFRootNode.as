package cc.minos.game.go.node
{
	
	/**
	 * ...
	 * @author Minos
	 */
	public class SGFRootNode extends SGFNode
	{
		
		public function SGFRootNode( node:SGFNode = null )
		{
			super( node );
		}
		
		override public function isRootNode():Boolean
		{
			return true;
		}
	
	}

}