package cc.minos.game.go.node
{
	import cc.minos.game.go.board.Board;
	
	/**
	 * ...
	 * @author Minos
	 */
	public class SGFTree extends Object
	{
		public var isVar:Boolean = false;
		public var isSinaSGF:Boolean = false;
		public var lastNode:SGFNode = null;
		public var nodes:Array;
		public var commentIndex:int = 0;
		public var handID:int = 0;
		public var root:SGFRootNode = null;
		public var isOK:Boolean = false;
		public var sgf:String = null;
		public var parent:SGFTree;
		
		public function SGFTree( tree:SGFTree = null )
		{
			parent = tree;
			nodes = [];
			root = new SGFRootNode( null );
			root.setNodePos( Board.EMPTY , -1 , -1 );
			lastNode = root;
		}
		
		public function toString():String
		{
			return "t: " + root.child;
		}
	
	}

}