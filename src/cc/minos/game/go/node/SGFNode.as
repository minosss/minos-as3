package cc.minos.game.go.node
{
	import cc.minos.game.go.board.Board;
	//import cc.minos.game.go.node.SGFVO;
	import cc.minos.game.go.SGFUtil;
	import flash.utils.Dictionary;
	
	/**
	 * ...
	 * @author Minos
	 */
	public class SGFNode
	{
		public var parent:SGFNode;
		public var child:SGFNode;
		public var isParsered:Boolean = false;
		public var nodeColor:int;
		public var trees:Array;
		public var handid:int;
		public var nodeX:int = -1;
		public var nodeY:int = -1;
		//
		public var setupArray:Array;
		//
		public var props:Dictionary;
		public var comment:Array;
		
		public function SGFNode( node:SGFNode )
		{
			trees = [];
			comment = [];
			setupArray = [];
			props = new Dictionary();
			parent = node;
			child = null;
			if ( parent != null )
			{
				handid = parent.handid + 1;
				parent.child = this;
			}
			else
			{
				handid = 0;
			}
		}
		
		public function addNodeProperty( type:String , data:String ):void
		{
			switch ( type )
			{
				case "B": 
					nodeX = getPos( data , 0 );
					nodeY = getPos( data , 1 );
					nodeColor = Board.BLACK;
					props[ type ] = data;
					break;
				case "W": 
					nodeX = getPos( data , 0 );
					nodeY = getPos( data , 1 );
					nodeColor = Board.WHITE;
					props[ type ] = data;
					break;
				case "C": 
					comment.push( data );
					break;
				case "AB": 
				case "AW": 
					setManual( type , data );
					break;
				default: 
					props[ type ] = data;
			}
		
		}
		
		protected function setManual( type:String , data:String ):void
		{
			var color:int = Board.EMPTY;
			
			if ( type == "AB" )
				color = Board.BLACK;
			else if ( type == "AW" )
				color = Board.WHITE;
			
			var vo:SGFVO;
			
			var pos:Array = data.split( "," );
			if ( !pos )
			{
				return;
			}
			
			for ( var i:int = 0 , len:int = pos.length ; i < len ; i++ )
			{
				var vos:Array = pos[ i ].split( ":" );
				if ( vos && vos.length > 1 )
				{
					var ax:int = getPos( vos[ 0 ] , 0 );
					var ay:int = getPos( vos[ 0 ] , 1 );
					var bx:int = getPos( vos[ 1 ] , 0 );
					var by:int = getPos( vos[ 1 ] , 1 );
					for ( var j:int = ay ; j <= by ; j++ )
					{
						for ( var k:int = ax ; k <= ax ; k++ )
						{
							vo = new SGFVO();
							vo.nodeColor = color;
							vo.nodeX = k;
							vo.nodeY = j;
							setupArray.push( vo );
							
						}
					}
				}
				else
				{
					vo = new SGFVO();
					vo.nodeColor = color;
					vo.nodeX = getPos( pos[ i ] , 0 );
					vo.nodeY = getPos( pos[ i ] , 1 );
					setupArray.push( vo );
				}
			}
		}
		
		protected function getPos( data:String , number:Number ):int
		{
			if ( data.length < 2 )
				return -1;
			data = data.toLowerCase();
			return SGFUtil.l2Int( data.charAt( number ) );
		}
		
		public function isRootNode():Boolean
		{
			return false;
		}
		
		public function setNodePos( color:int , xpos:int , ypos:int ):void
		{
			nodeColor = color;
			nodeX = xpos;
			nodeY = ypos;
		}
		
		public function toString():String
		{
			return "[ x:" + SGFUtil.i2Letter(nodeX) +", y:" + SGFUtil.i2Letter(nodeY) + ", c:" + nodeColor +" ]";
		}
	
	}

}