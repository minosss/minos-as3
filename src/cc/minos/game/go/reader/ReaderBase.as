package cc.minos.game.go.reader
{
	import cc.minos.game.go.node.SGFTree;
	import cc.minos.game.go.SGFUtil;
	//import cc.minos.mgo.node.SGFTree;
	//import cc.minos.mgo.SGFUtil;
	
	/**
	 * ...
	 * 读谱基类
	 * @author Minos
	 */
	public class ReaderBase
	{
		public static const STRICT_SGF:String = "strict_sgf";
		public static const LAX_SGF:String = "lax_sgf";
		public static const EOF:String = "eof";
		
		//棋谱
		protected var sgf:String;
		//当前字符
		protected var head:String;
		//当前位置
		protected var sgfIndex:int = -1;
		//
		public var rootTree:SGFTree;
		//
		protected var _roads:Array;
		//是否设置
		public var isParsered:Boolean = false;
		
		public function ReaderBase()
		{
			_roads = [];
		}
		
		/**
		 * 下个字符
		 * @param	s
		 * @return
		 */
		protected function toNext( s:String ):Boolean
		{
			if ( s != head )
				return false;
			nextToken();
			return true;
		}
		
		/**
		 * 下个字符
		 */
		protected function nextToken():void
		{
			do
			{
				sgfIndex++;
				if ( sgfIndex >= sgf.length )
				{
					head = EOF;
					return;
				}
				head = sgf.charAt( sgfIndex );
			} while ( SGFUtil.isWhitespace( head ) )
		}
		
		/**
		 * 设置棋谱
		 * @param	tree
		 * @param	s
		 */
		public function setSGF( tree:SGFTree , s:String ):void
		{
		}
		
		public function getRoads():Array
		{
			return _roads;
		}
	}

}