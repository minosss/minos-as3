package cc.minos.game.go.reader
{
	import cc.minos.game.go.SGFUtil;
	
	/**
	 * ...
	 * @author Minos
	 */
	public class ReaderFactory
	{
		private static var sgf:String;
		private static var head:String;
		private static var index:int = -1;
		
		public function ReaderFactory()
		{
		}
		
		/**
		 *
		 * @param	s
		 * @return
		 */
		public static function getReader( s:String ):ReaderBase
		{
			var reader:ReaderBase;
			index = -1;
			sgf = s;
			nexttoken();
			if ( SGFUtil.match( head , "(" ) )
			{
				nexttoken();
				if ( SGFUtil.match( head , ";" ) )
				{
					reader = new ReaderSGF();
				}
				else
				{
					//reader = new ReaderSGFSina();
				}
			}
			else if ( SGFUtil.match( head , "\\" ) )
			{
				//reader = new ReaderGIB();
			}
			else
			{
				//reader = new ReaderNGF();
			}
			return reader;
		}
		
		private static function nexttoken():void
		{
			do
			{
				index++;
				if ( index >= sgf.length )
				{
					head = null;
					return;
				}
				head = sgf.charAt( index );
			} while ( SGFUtil.isWhitespace( head ) )
		}
	
	}

}