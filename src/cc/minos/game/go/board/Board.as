package cc.minos.game.go.board
{
	import flash.events.Event;
	import flash.geom.Point;
	
	/**
	 * ...
	 * 棋盘基类
	 * @author Minos
	 */
	public class Board
	{
		public static const BLACK:int = -1;
		public static const WHITE:int = 1;
		public static const EMPTY:int = 0;
		
		protected var rows:int;
		protected var cols:int;
		
		protected var offsets:Array;
		protected var lastMove:Point;
		protected var chess:Array;
		
		public function Board( chess:Array = null )
		{
			this.chess = chess;
			init();
		}
		
		protected function init():void
		{
			if ( chess == null )
			{
				rows = cols = 9;
				chess = Board.createEmptyChess( rows , cols );
			}
			else
			{
				rows = chess[ 0 ].length;
				cols = chess.length;
			}
			lastMove = new Point( -1 , -1 );
		}
		
		/**
		 * 得到棋盘上某个位置的状态
		 * @param	row
		 * @param	col
		 * @return
		 */
		public function getPointContent( row:int , col:int ):int
		{
			return chess[ row ][ col ];
		}
		
		public function makeMove( color:int , row:int , col:int ):void
		{
		}
		
		public function findEnemiesCanKill( color:int , row:int , col:int ):Array
		{
			return null;
		}
		
		/**
		 * 创建空棋盘数组
		 * @param	rows
		 * @param	cols
		 * @return
		 */
		public static function createEmptyChess( rows:int , cols:int ):Array
		{
			var ary:Array = [];
			for ( var i:int = 0 ; i < rows ; i++ )
			{
				ary[ i ] = [];
				for ( var j:int = 0 ; j < cols ; j++ )
				{
					ary[ i ][ j ] = EMPTY;
				}
			}
			return ary;
		}
	
	}

}