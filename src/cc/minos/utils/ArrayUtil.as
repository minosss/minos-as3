package cc.minos.utils
{
	import flash.utils.ByteArray;
	
	/**
	 * ...
	 * 数组处理
	 * @author Minos
	 */
	public class ArrayUtil
	{
		
		public static function getMaxFromArray( ary:Array ):Number
		{
			return Math.max.apply( null, ary ) as Number;
		}
		
		public static function getMinFromArray( ary:Array ):Number
		{
			return Math.min.apply( null, ary ) as Number;
		}
		
		/**
		 * 合并
		 * @param	a1
		 * @param	a2
		 */
		public static function merge( a1:Array, a2:Array ):void
		{
			for ( var i:int = 0; i < a2.length; i++ )
			{
				if ( a2[ i ] != null )
					a1.push( a2[ i ] );
			}
		}
		
		/**
		 * 随机取出数组
		 * @param	array			:	数组
		 * @param	length			:	个数
		 * @param	isCopy			:
		 * @return	取出的随机数组
		 */
		public static function splicing( array:Array, length:int, isCopy:Boolean = false ):Array
		{
			if ( isCopy )
				array = copyArray( array );
			var shuffled:Array = new Array( length );
			var randomPos:Number = 0;
			for ( var i:int = 0; i < length; i++ )
			{
				randomPos = int( Math.random() * array.length );
				shuffled[ i ] = array.splice( randomPos, 1 )[ 0 ];
			}
			return shuffled;
		}
		
		/**
		 * 随机数组
		 * @param	array			:	数组
		 * @return	打乱的数组
		 */
		public static function sorting( array:Array ):Array
		{
			function randomSort( a:*, b:* ):Number
			{
				if ( Math.random() < 0.5 )
					return -1;
				else
					return 1;
			}
			return array.sort( randomSort );
		}
		
		/**
		 * 判断数组中是否有数据
		 * @param	arr				:	数组
		 * @param	value			:	对象
		 * @return	布尔值
		 */
		public static function containsValue( arr:Array, value:Object ):Boolean
		{
			return ( arr.indexOf( value ) != -1 );
		}
		
		/**
		 * 删除数组中的数据
		 * @param	arr				:	数组
		 * @param	value			:	对象
		 */
		public static function removeValue( arr:Array, value:Object ):void
		{
			var len:uint = arr.length;
			
			for ( var i:Number = len; i > -1; i-- )
			{
				if ( arr[ i ] === value )
				{
					arr.splice( i, 1 );
				}
			}
		}
		
		/**
		 * 筛选出唯一数据的数组
		 * @example	a=[1,1,2,3,4,5,5] => n = [1,2,3,4,5]
		 * @param	a				:	原数组
		 * @return	返回唯一数据的数组
		 */
		public static function createUniqueCopy( a:Array ):Array
		{
			var newArray:Array = new Array();
			
			var len:Number = a.length;
			var item:Object;
			
			for ( var i:uint = 0; i < len; ++i )
			{
				item = a[ i ];
				
				if ( ArrayUtil.containsValue( newArray, item ) )
				{
					continue;
				}
				
				newArray.push( item );
			}
			
			return newArray;
		}
		
		/**
		 * 复制数组
		 * @param	arr				:	数组
		 * @return	数组
		 */
		public static function copyArray( arr:Array ):Array
		{
			return arr.slice();
		}
		
		/**
		 * 判断两个数组是否完全相同
		 * @param	arr1			:	数组
		 * @param	arr2			:	数组
		 * @return	布尔值
		 */
		public static function arraysAreEqual( arr1:Array, arr2:Array ):Boolean
		{
			if ( arr1.length != arr2.length )
			{
				return false;
			}
			
			var len:Number = arr1.length;
			
			for ( var i:Number = 0; i < len; i++ )
			{
				if ( arr1[ i ] !== arr2[ i ] )
				{
					return false;
				}
			}
			
			return true;
		}
	
	}

}