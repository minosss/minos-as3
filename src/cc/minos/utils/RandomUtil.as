package cc.minos.utils
{
	
	/**
	 * ...
	 * @author Minos
	 */
	public class RandomUtil
	{
		
		/**
		 * 随机两个数之间的一个数
		 * @param	min
		 * @param	max
		 * @return
		 */
		public static function randomNumber( min:Number , max:Number ):Number
		{
			if ( min > max )
			{
				var tmp:Number = max;
				max = min;
				min = tmp;
			}
			return Math.random() * ( max - min ) + min;
		}
		
		/**
		 * 随机两个数之间的一个整数
		 * @param	min
		 * @param	max
		 * @return
		 */
		public static function randomInt( min:int , max:int ):int
		{
			return Math.round( randomNumber( min , max ) );
		}
		
		//默认随机字符
		public static const ALPHABET:String = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		
		/**
		 * 随机字符
		 * @param	length			:	长度
		 * @param	string			:	字符（默认为a-zA-Z0-9)
		 * @return	返回随机字符串
		 */
		public static function randomString( length:int , string:String = null ):String
		{
			if ( string == null )
				string = ALPHABET;
			var result:String = "";
			while ( length-- )
			{
				result += string.charAt( Math.floor( Math.random() * string.length ) );
			}
			return result;
		}
	
	}

}