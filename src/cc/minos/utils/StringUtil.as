package cc.minos.utils
{
	
	/**
	 * ...
	 * 字符串处理
	 * @author Minos
	 */
	public class StringUtil
	{

		/**
		 * 截取路径
		 * @param	url
		 * @param	sign
		 * @return
		 */
		public static function getPath( url:String = "" , sign:String = "/" ):String
		{
			return url.substring( 0 , url.lastIndexOf( sign ) + 1 );
		}
		
		/**
		 * 截取特定字符中间的字符串
		 * @param	s
		 * @param	sign
		 * @return
		 */
		public static function substringBySign( s:String , sign:String = "_" ):String
		{
			return s.substring( s.indexOf( sign ) + 1 , s.lastIndexOf( sign ) );
		}
		
		/**
		 * 判断两个字符串是否相等
		 * @param	s1
		 * @param	s2
		 * @param	caseSensitive
		 * @return	返回布尔值
		 */
		public static function stringsAreEqual( s1:String , s2:String , caseSensitive:Boolean ):Boolean
		{
			if ( caseSensitive )
			{
				return ( s1 == s2 );
			}
			else
			{
				return ( s1.toUpperCase() == s2.toUpperCase() );
			}
		}
		
		/**
		 * 去除前后的空格
		 * @param	input
		 * @return	返回去除空格的字符串
		 */
		public static function trim( input:String ):String
		{
			return StringUtil.ltrim( StringUtil.rtrim( input ) );
		}
		
		/**
		 * 去除开头的空格
		 * @param	input
		 * @return	返回去除开头空格的字符串
		 */
		public static function ltrim( input:String ):String
		{
			var size:Number = input.length;
			for ( var i:Number = 0 ; i < size ; i++ )
			{
				if ( input.charCodeAt( i ) > 32 )
				{
					return input.substring( i );
				}
			}
			return "";
		}
		
		/**
		 * 去除结尾的空格
		 * @param	input
		 * @return	返回去除结尾空格的字符串
		 */
		public static function rtrim( input:String ):String
		{
			var size:Number = input.length;
			for ( var i:Number = size ; i > 0 ; i-- )
			{
				if ( input.charCodeAt( i - 1 ) > 32 )
				{
					return input.substring( 0 , i );
				}
			}
			
			return "";
		}
		
		/**
		 * 替换
		 * @param	input				:	原字符串
		 * @param	replace				:	替换的字符串
		 * @param	replaceWith			:	添加的字符串
		 * @return	返回替换后的字符串
		 */
		public static function replace( input:String , replace:String , replaceWith:String ):String
		{
			return input.split( replace ).join( replaceWith );
		}
		
		/**
		 * 删除
		 * @param	input				:	原字符串
		 * @param	remove				:	删除的字符串
		 * @return	返回删除后的字符串
		 */
		public static function remove( input:String , remove:String ):String
		{
			return StringUtil.replace( input , remove , "" );
		}
		
		/**
		 * 是否为空
		 * @param	s
		 * @return	返回布尔值
		 */
		public static function stringHasValue( s:String ):Boolean
		{
			return ( s != null && s.length > 0 );
		}
		
		/**
		 * 前缀
		 * @param	input				:	原字符串
		 * @param	prefix				:	前缀
		 * @return	返回布尔值
		 */
		public static function beginsWith( input:String , prefix:String ):Boolean
		{
			return ( prefix == input.substring( 0 , prefix.length ) );
		}
		
		/**
		 * 后缀
		 * @param	input				:	原字符串
		 * @param	suffix				:	后缀
		 * @return	返回布尔值
		 */
		public static function endsWith( input:String , suffix:String ):Boolean
		{
			return ( suffix == input.substring( input.length - suffix.length ) );
		}
		
	}

}