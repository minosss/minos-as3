package cc.minos.utils
{
	
	/**
	 * ...
	 * @author Minos
	 */
	public class ColorUtil
	{
		
		/**
		 *
		 * @param	number
		 * @param	lenght
		 * @param	showHexDenotation
		 * @return
		 */ /*public static function numberToHex( number:uint, lenght:uint = 1, showHexDenotation:Boolean = true ):String
		   {
		   var s:String = number.toString( 16 ).toUpperCase();
		   while ( lenght > s.length )
		   {
		   s = "0" + s;
		   }
		   if ( showHexDenotation )
		   s = "0x" + s;
		   return s;
		 }*/
		
		/**
		 * rgb值转hex
		 * @param	r	:	red ( 0-255 )
		 * @param	g	:	green ( 0-255 )
		 * @param	b	:	blue ( 0-255 )
		 * @return	16 hex string
		 */
		public static function rgbToHex( r:int, g:int, b:int ):String
		{
			return (( r << 16 ) | ( g << 8 ) | b ).toString( 16 ).toUpperCase();
		}
	
	}

}