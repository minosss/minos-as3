package cc.minos.utils
{
	import flash.utils.Dictionary;
	
	/**
	 * ...
	 * 字典处理
	 * @author Minos
	 */
	public class DictionaryUtil
	{
		
		/**
		 * 获取字典中键的数组
		 * @param	d
		 * @return
		 */
		public static function getKeys( d : Dictionary ) : Array
		{
			var a : Array = new Array();
			for ( var key : Object in d )
			{
				a.push( key );
			}
			return a;
		}
		
		/**
		 * 获取字典中值的数组
		 * @param	d
		 * @return
		 */
		public static function getValues( d : Dictionary ) : Array
		{
			var a : Array = new Array();
			for each ( var value : Object in d )
			{
				a.push( value );
			}
			return a;
		}
	
	}
}