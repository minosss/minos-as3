package cc.minos.utils
{
	import flash.display.InteractiveObject;
	import flash.system.Capabilities;
	import flash.ui.ContextMenu;
	import flash.ui.ContextMenuItem;
	
	/**
	 * ...
	 * @author Minos
	 */
	public class VersionUtil
	{
		/* 右键版本信息 */
		
		private static const NOW_COMPANY:String = "@EE";
		
		public static function apply( obj:InteractiveObject, vo:Object, project:String = "" ):void
		{
			var menu:ContextMenu = obj.contextMenu;
			
			if ( !menu )
			{
				menu = new ContextMenu();
				menu.hideBuiltInItems();
			}
			
			var _version:String = project + "v" + vo.Major + "." + vo.Minor + "." + vo.Build + "." + vo.Revision + " BY " + vo.Author + NOW_COMPANY;
			var _item:ContextMenuItem = new ContextMenuItem( _version.toUpperCase(), false, false );
			menu.customItems.push( _item );
			
			obj.contextMenu = menu;
		}
		
		/* 获取flashplaer版本 */
		
		public static function getFlashPlayerVersion():Number
		{
			var versionString:String = Capabilities.version;
			var pattern:RegExp = /^(\w*) (\d*),(\d*),(\d*),(\d*)$/;
			var result:Object = pattern.exec( versionString );
			if ( result != null )
			{
				//	trace("input: " + result.input);
				//	trace("platform: " + result[1]);
				//	trace("majorVersion: " + result[2]);
				//	trace("minorVersion: " + result[3]);    
				//	trace("buildNumber: " + result[4]);
				//	trace("internalBuildNumber: " + result[5]);
				return Number( result[ 2 ] + "." + result[ 3 ] );
			}
			else
			{
				return 0;
			}
		}
	
	}

}