package cc.minos.utils
{
	
	public class MathUtil extends Object
	{
		
		
		/**
		 * 弧度
		 * @param	degrees
		 * @return
		 */
		public static function toRadians( degrees:Number ):Number
		{
			return degrees * Math.PI / 180;
		}
		
		/**
		 * 角度
		 * @param	radians
		 * @return
		 */
		public static function toDegrees( radians:Number ):Number
		{
			return radians * 180 / Math.PI;
		}
		
		/**
		 * 最大公约数
		 * @param	a
		 * @param	b
		 * @return
		 */
		public static function gcd( a:Number , b:Number ):Number
		{
			if ( a == 0 )
				return b;
			var r:Number;
			while ( b != 0 )
			{
				r = a % b;
				a = b;
				b = r;
			}
			return Math.abs( a );
		}
		
		/**
		 * 最小公倍数
		 * @param	a
		 * @param	b
		 * @return
		 */
		public static function lcm( a:Number , b:Number ):Number
		{
			var ta:Number = a;
			var tb:Number = b;
			var tr:Number;
			do
			{
				if (( tr = a % b ) == 0 )
					break;
				a = b;
				b = tr;
			} while ( 1 );
			
			return ta * tb / b;
		}
		
		/**
		 * 平均值
		 * @param	arr
		 * @return
		 */
		public static function mean( arr:Array ):Number
		{
			var l:Number = arr.length;
			var s:Number = 0;
			while ( l-- )
				s += arr[ l ];
			return s / arr.length;
		}
		
		/**
		 * 方差
		 * @param	arr		:	数字列表
		 * @return	返回数字列表的方差
		 */
		public static function variance( arr:Array ):Number
		{
			var x:Number = 0;
			var x2:Number = 0;
			var len:Number = arr.length;
			var j:Number;
			
			for ( j = 0 ; j < len ; j++ )
			{
				x += arr[ j ];
				x2 += arr[ j ] * arr[ j ];
			}
			
			return (( len * x2 ) - ( x * x ) ) / ( len * ( len - 1 ) );
		}
	}
}