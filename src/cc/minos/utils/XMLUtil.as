package cc.minos.utils
{
	
	/**
	 * ...
	 * XML处理
	 * @author Minos
	 */
	public class XMLUtil extends Object
	{
		public static const TEXT : String = "text";
		public static const COMMENT : String = "comment";
		public static const PROCESSING_INSTRUCTION : String = "processing-instruction";
		public static const ATTRIBUTE : String = "attribute";
		public static const ELEMENT : String = "element";
		
		/**
		 * 添加CDATA格式
		 * @param	title			:	节点名
		 * @param	content			:	内容
		 * @return
		 */
		public static function CDATA( title : String = "item" , content : String = "" ) : XML
		{
			return new XML( "<" + title + "><![CDATA[" + content + "]]></" + title + ">" );
		}
		
		/**
		 * 判断字符串是否XML
		 * @param	data				:	字符串数据
		 * @return	返回布尔值
		 */
		public static function isValidXML( data : String ) : Boolean
		{
			var xml : XML;
			try
			{
				xml = new XML( data );
			}
			catch ( e : Error )
			{
				return false;
			}
			if ( xml.nodeKind() != XMLUtil.ELEMENT )
			{
				return false;
			}
			return true;
		}
		
		/**
		 * 返回下一个节点
		 * @param	x				:	子节点
		 * @return	返回XML
		 */
		public static function getNextSibling( x : XML ) : XML
		{
			return XMLUtil.getSiblingByIndex( x , 1 );
		}
		
		/**
		 * 返回上一个节点
		 * @param	x				:	子节点
		 * @return	返回XML
		 */
		public static function getPreviousSibling( x : XML ) : XML
		{
			return XMLUtil.getSiblingByIndex( x , -1 );
		}
		
		/**
		 * 获取节点
		 * @param	x				:	子节点
		 * @param	count			:	序号
		 * @return	返回XML
		 */
		protected static function getSiblingByIndex( x : XML , count : int ) : XML
		{
			var out : XML;
			try
			{
				out = x.parent().children()[ x.childIndex() + count ];
			}
			catch ( e : Error )
			{
				return null;
			}
			return out;
		}
	
	}

}